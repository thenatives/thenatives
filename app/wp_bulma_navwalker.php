<?php
/*
* Original https://www.microdot.io/simpler-wp-nav-menu-markup/
*/
class wp_bulma_navwalker extends Walker_Nav_Menu {

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		$classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }

        $active_class = '';
        if( in_array('current-menu-item', $classes) ) {
            $active_class = ' is-active"';
        } else if( in_array('current-menu-parent', $classes) ) {
            $active_class = ' "is-active-parent"';
        } else if( in_array('current-menu-ancestor', $classes) ) {
            $active_class = ' "is-active-ancestor"';
        }

        // Append css classes to element 
        $element_class = $args->menu_class . ' ' . $active_class;

        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
        }

        $output .= '<a class="'. $element_class . '" href="' . $url . '">' . $item->title . '</a>'; 
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "\n";
	}
}
