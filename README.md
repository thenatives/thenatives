
# TheNatives Best Practices
Please use the admin email address '*clientadmin@thenatives.com.au*' when setting up Wordpress and configuring all email addresses (contact forms etc) that will be replaced with the client's email address on Production.

External JS Components must be ES6 Compliant.

**Do not install Visual Composer, or any Wordpress plugins for building visual interfaces. Client's never use them and they are a pain for developers.**

# [Sage](https://roots.io/sage/)
[![Packagist](https://img.shields.io/packagist/vpre/roots/sage.svg?style=flat-square)](https://packagist.org/packages/roots/sage)
[![devDependency Status](https://img.shields.io/david/dev/roots/sage.svg?style=flat-square)](https://david-dm.org/roots/sage#info=devDependencies)
[![Build Status](https://img.shields.io/travis/roots/sage.svg?style=flat-square)](https://travis-ci.org/roots/sage)

Sage is a WordPress starter theme with a modern development workflow.

## Features

* Sass for stylesheets
* Modern JavaScript
* [Webpack](https://webpack.github.io/) for compiling assets, optimizing images, and concatenating and minifying files
* [Browsersync](http://www.browsersync.io/) for synchronized browser testing
* [Blade](https://laravel.com/docs/5.5/blade) as a templating engine
* [Controller](https://github.com/soberwp/controller) for passing data to Blade templates
* CSS framework [Bulma](https://bulma.io/)
* CSS framework [Bulma Extensions](https://bulma.io/extensions/)
* Font Awesome

## Requirements

Make sure all dependencies have been installed before moving on:

* [WordPress](https://wordpress.org/) >= 4.7
* [PHP](https://secure.php.net/manual/en/install.php) >= 7.1.3 (with [`php-mbstring`](https://secure.php.net/manual/en/book.mbstring.php) enabled)
* [Composer](https://getcomposer.org/download/)
* [Node.js](http://nodejs.org/) >= 6.9.x
* [Yarn](https://yarnpkg.com/en/docs/install)

## Theme Configuration

Install Sage using Composer from your WordPress themes directory (replace `your-theme-name` below with the name of your theme):

```shell
# @ wp-content/themes/<themename>
$ composer install
```

During theme installation you will have options to update `style.css` theme headers, select a CSS framework, add Font Awesome, and configure Browsersync.

## Theme structure

```shell
themes/your-theme-name/     # → Root of your Sage based theme
├── app/                    # → Theme PHP
│   ├── controllers/        # → Controller files
│   ├── admin.php           # → Theme customizer setup
│   ├── filters.php         # → Theme filters
│   ├── helpers.php         # → Helper functions
│   └── setup.php           # → Theme setup
├── composer.json           # → Autoloading for `app/` files
├── composer.lock           # → Composer lock file (never edit)
├── dist/                   # → Built theme assets (never edit)
├── node_modules/           # → Node.js packages (never edit)
├── package.json            # → Node.js dependencies and scripts
├── resources/              # → Theme assets and templates
│   ├── assets/             # → Front-end assets
│   │   ├── config.json     # → Settings for compiled assets
│   │   ├── build/          # → Webpack and ESLint config
│   │   ├── fonts/          # → Theme fonts
│   │   ├── images/         # → Theme images
│   │   ├── scripts/        # → Theme JS
│   │   │    ├── autoload/  # → External JS Components / Plugins
|   |   |────vue            # → Vue JS Components
│   │   └── styles/         # → Theme stylesheets
│   ├── functions.php       # → Composer autoloader, theme includes
│   ├── index.php           # → Never manually edit
│   ├── screenshot.png      # → Theme screenshot for WP admin
│   ├── style.css           # → Theme meta information
│   └── views/              # → Theme templates
│       ├── layouts/        # → Base templates
│       └── partials/       # → Partial templates
└── vendor/                 # → Composer packages (never edit)
```

## Theme setup

Edit `app/setup.php` to enable or disable theme features, setup navigation menus, post thumbnail sizes, and sidebars.

## Theme development

* Run `composer install` from the theme directory to install dependencies
* Run `yarn` from the theme directory to install NPM dependencies
* Update `resources/assets/config.json` settings:
  * `devUrl` should reflect your local development hostname
  * `publicPath` should reflect your WordPress folder structure (`/wp-content/themes/sage`)

### Build commands

* `yarn start` — Compile assets when file changes are made, start Browsersync session
* `yarn build` — Compile and optimize the files in your assets directory
* `yarn build:production` — Compile assets for production

### Vue.js

* Uses NPM Axios library to process all ajax requests.
* See file `resources/assets/scripts/common.js` for an example
* Put all ajax requests in a seperate Wordpress plugin called `themename-theme-extensions`
* For an example of how to use the Wordpress REST api with this theme, see the following [example](https://bitbucket.org/thenatives/wordpress-rest-example)

### Using ACF & Controllers
Sage uses a Controller to [SoberWP](https://github.com/soberwp/controller) handle all logic/acf fields in blade templates.

Controllers must follow the [Wordpress Template Hierachy](https://developer.wordpress.org/themes/basics/template-hierarchy/) 

#### Basic Controller
The following example will expose the ACF field `$images` to `resources/views/single.blade.php`

**app/Controllers/Single.php**
```php
<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Single extends Controller
{
    /**
     * Return images from Advanced Custom Fields
     *
     * @return array
     */
    public function images()
    {
        return get_field('images');
    }
}
```

**resources/views/single.blade.php**
```php
@if($images)
  <ul>
    @foreach($images as $image)
      <li><img src="{{$image['sizes']['thumbnail']}}" alt="{{$image['alt']}}"></li>
    @endforeach
  </ul>
@endif
```

### Wordpress Plugins

 * Use **[Formiddable Forms](formidableforms.com)** as we have a license
 * **Advanced Custom Fields**
   * ACF is configured to hide the admin menu from non admin users. See `app/setup.php` for a list of users. You might need to add your username.
   * ACF will save all fields locally to `app/acf` so all fields are under source control, and there is no need to sync them across environments.
     * [Local Json Explained](https://www.advancedcustomfields.com/resources/local-json/)
     * [Synchronized JSON](https://www.advancedcustomfields.com/resources/synchronized-json/)

* [Mailgun](https://wordpress.org/plugins/mailgun/)
* [WP Migrate Pro](https://deliciousbrains.com/wp-migrate-db-pro/)
* [Instagram](https://github.com/scottsweb/wp-instagram-widget) It will create a widget to inject into the site

## Continuous Integration
We use Bitbucket Pipelines to push code from Bitbucket onto our Test/Staging/Production servers.
See `bitbucket-pipelines.yml` in the root directory of your project.

 - https://confluence.atlassian.com/bitbucket/php-with-bitbucket-pipelines-873907835.html

## How to sync the database (Development, Staging, Production)
* [WP Migrate Pro Quick Start Guide](https://deliciousbrains.com/wp-migrate-db-pro/doc/quick-start-guide/)

## Documentation

* [Sage documentation](https://roots.io/sage/docs/)
* [Controller documentation](https://github.com/soberwp/controller#usage)

## Community

Keep track of development and community news.

* Participate on the [Roots Discourse](https://discourse.roots.io/)
* Follow [@rootswp on Twitter](https://twitter.com/rootswp)
* Read and subscribe to the [Roots Blog](https://roots.io/blog/)
* Subscribe to the [Roots Newsletter](https://roots.io/subscribe/)
* Listen to the [Roots Radio podcast](https://roots.io/podcast/)
