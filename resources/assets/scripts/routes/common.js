import Vue from 'vue'
import AOS from 'aos'

// Import Vue Components
import exampleComponent from '../vue/exampleComponent.vue'
import carousel from '../vue/carousel.vue'


// Import Insight
import * as insight from '../insight.js';

// Config Settings for Axios
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export default {
  init() {
    // JavaScript to be fired on all pages
    
    // Load a single instance of Vue.js
    // @{{ message }}
    // <example-component></example-component>, 
    new Vue({
      el: "#app",
      name: "app",
      data: {
        message: "Vue.js Hello World",
        siteUrl: document.querySelector("#app").dataset.siteurl,
      },
      components: {
        exampleComponent,
        carousel,
      },
    })
    
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

    // Initalise AOS
    AOS.init({
      disable: 'mobile',
      duration: 700,
    });
    
    // Fire off custom tracking events for GA, Active Campaign etc
    insight.trackEvents();
  },
};
