<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  {{-- 
      // This is used to authenticate the user with the REST API
      // https://developer.wordpress.org/rest-api/using-the-rest-api/authentication/
    --}}
  <meta id="nonce" name="nonce" value="{{ wp_create_nonce( 'wp_rest' ) }}"/>

  @php(wp_head())
</head>
