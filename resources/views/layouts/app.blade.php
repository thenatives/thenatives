<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')

  <body @php(body_class())>
    @php(do_action('get_header'))
    @include('partials.header')

    <div class="wrap container" role="document">

      <div id="app" class="content" data-siteurl="{{ site_url() }}">
        <main class="main">
          
          {{-- Example Vue.Js Component --}}
          <example-component></example-component>

          {{-- Example Vue.Js Carousel --}}
          {{-- <carousel>
            <img src="https://placeimg.com/640/480/any?1">
            <img src="https://placeimg.com/640/480/any?2">
            <img src="https://placeimg.com/640/480/any?3">
            <img src="https://placeimg.com/640/480/any?4">
            <img src="https://placeimg.com/640/480/any?5">
          </carousel> --}}

          @yield('content')
        </main>

        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>

    @php(do_action('get_footer'))
    @include('partials.footer')
    @php(wp_footer())

  </body>
</html>
